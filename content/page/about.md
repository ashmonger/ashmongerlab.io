---
title: About me
subtitle: What am I doing with my time?
comments: false
---

I work in the IT field since 2007.

Actually writing puppet code for a living and trying to make video games for fun.

I'm also a drinker of _damn fine coffee_ and other spirits.
