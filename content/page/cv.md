---
title: Curriculum Vitae
subtitle: What have I done?
comments: false
---

## Compétences techniques

* Hardware: PC, Serveurs, Routers (Cisco - notions), Switchs (HP, Cisco)
* Linux      & Ubuntu, Debian, RHEL, CentOS
* Microsoft: Windows 9x, XP, 2003 Server, 7, 2008 server R2, 10
* Réseau      & Dns, dhcp, Samba, nfs, ftp, web
* Sécurité    & Iptables, vpn
* Programmation    & Shell (Bash), Python, \LaTeX, html, css
* %Mail      & Exchange, Postfix
* %Monitoring    & Nagios, Centreon
* %Cluster    & Heartbeat, ocfs, RedHat Cluster, VMware ESXi Cluster
* Virtualisation    & VMware ESXi, Virtualbox, Docker
* Gestion de code & Git, Gitlab, Mercurial
* CI/CD & Gitlab-ci, Docker
* %Base de données  & MySQL
* %Rédaction    & Documentation, Procedures
* %Gestion    & Netcenter, incidents techniques, diagnostiques


## Expérience Professionnelle
### Consort NT, La Poste
_Depuis Juillet 2016, Nantes_

* __Contexte__
  * Au sein du département du Traitement Automatisé de l'Enveloppe,
chargé de la migration des solutions actuelles vers des outils plus récents.

__Poste__
: Ingénieur systèmes

* __Mission__
  * Etude et mise en place d'un outils de CMDB (GLPI)
  * Mise en place de scripts bash pour historisation des connexions utilisateurs
  * Migration de script Ruby en Bash pour création de paquets de clés SSH
  * Mise en place d'un Gitlab avec CI et Docker intégré
  * Création de modules et mise à jour du code Puppet
* __Env. Technique__
  * Linux, Ubuntu, Shell, GLPI, Git, Grand Compte, Jenkins, Docker, Puppet

### Open, Domisys
_Mai 2016 à Juin 2016, Nantes_

* Poste ?
* Mission
  * Etude de la solution GLPI/OCS actuelle
  * Mise en place de tests et recettes pour mise à jour de la solution vers une utilisation optimale et plus complète de l'outil GLPI et Fusioninventory
* Env. Technique    & Linux, Debian, GLPI, Git, DSI

### Open, DGA-MI
_Janvier 2015 à Mai 2016, Rennes_

*Contexte          & DGA Maîtrise de l'information est un centre technique
    de la DGA (Direction Générale de l'Armement) qui a pour mission des études,
    expertises et essais dans les domaines de la guerre électronique,
    des systèmes d'armes, des systèmes d'information, des télécommunications,
    de la sécurité de l'information et des composants électronique.
Poste      & Gestion technique de platformes operationnelles
Rôles et activités  & Administration et gestion de différentes plateformes d'expérimentation
    & Maintien en conditions opérationnelles des plateformes
    & Intégration de nouvelles plateformes au sein de l'architecture existante
    & Evolution des systèmes et réseaux des plateformes
    & Migration de plateforme physique vers plateforme virtualisée
Env. Technique    & Linux, Windows, Cisco, VMWare ESXi, Defense
 &


### Open, Beaumanoir
> Juin 2014 à Déc. 2014, Saint-Malo
Contexte    & Groupe de textile, spécialiste de la distribution mode
Poste      & Intégration et administration système
Rôles et activités  & Installation et maintien en condition opérationnelle de plateformes Linux
    & Intégration d'applications packagées sur middleware (Tomcat, Apache)
    & Mise en production et évolution des systèmes de monitoring : Nagios/Centreon
    & Normalisation du socle système et middleware
    & Installation et maintenance opérationnelle des applications internes (dev/pré-prod/prod)
    & Installation et maintenance opérationnelle du parc serveur linux (dev/pré-prod/prod)
    & Création de scripts d'industrialisation pour tâches récurrentes, installations et diagnostiques
    & Conseil sur mise en œuvre de nouvelle infrastructure
Env. Technique    & Linux, Debian, VMWare ESXi, Centreon, Bash, OpenLDAP, DHCP(AD, Linux), DNS (AD, BIND), PME
 &


### Open, DGA-MI
> Fév. 2013 à Sept. 2013, Rennes
Rôles et activités  & Administration et gestion de différentes plateformes d'expérimentation
    & Maintien en conditions opérationnelles des plateformes
    & Intégration de nouvelles plateformes au sein de l'architecture existante
    & Evolution des systèmes et réseaux des plateformes
    & Migration de plateforme physique vers plateforme virtualisée
Env. Technique    & Linux, Windows, Cisco, VMWare ESXi, Defense
 &


### Open, SNCF
> Mai 2012 à Nov. 2012, Lyon
Poste      & Ingénierie système socle
Projets    & Mise en place de script d'industrialisation pour installation et tâches récurrentes
    & Création de script de diagnostic et de traitement des données
Rôles et activités  & Installation et maintien en condition opérationnelle de plateformes Linux
    & Mise à disposition pour les clients de plateformes opérationnelles
    & Maintiens et évolution du socle système et applicatif
    & Diagnostics et résolution de problèmes machines et systèmes
    & Collaboration avec différents services (Middleware, BDD, exploitation) pour le suivi et la validation des machines
Env. Technique    & Redhat, SuSE, Debian, scripting, bash, ksh, awk, sed, grand compte
 &


### Open, SNCF
> Nov. 2011 à Mai 2012, Lyon
Poste      & Administration système
Rôles et activités   & Administration système Linux Redhat, Windows Server 2003
    & Gestion des serveurs DNS (Linux et Windows 2003) et DHCP
    & Déploiement d'applications packagées
    & Diagnostics et résolutions de problèmes liés aux plateformes d'hébergement des sites client
    & Maintien en condition opérationnelle des applications métiers
Env. Technique    & Redhat, Linux, Windows Server 2003, DNS, BIND, Apache, grand compte
 &


### Open, Cégélec
> Mai 2011 à Oct. 2011, Lyon
Contexte    & Les entreprises Cegelec conçoivent, installent et maintiennent des systèmes dans l’industrie, les infrastructures et le tertiaire, particulièrement dans des secteurs à forte demande comme par exemple l’énergie, le pétrole, le bâtiment et les travaux publics.
Poste      & Ingénierie système et réseaux
\textbf{Projet:~\textit{Stella & Création de l'infrastructure complète (système et réseau)
    & Installation Cluster VMWare ESXi
    & Installation et intégration de stockage SAN (HP MSA) pour le Cluster ESXi
    & Intégration de l'environnement réseau HP Procurve
    & Installation et mise à disposition de VMs Windows Server 2003
    & Gestion logistique du matériel (mise en baie et répartition homogène)
    & Intégration des applicatifs sur clients lourds
\textbf{Projet:~\textit{DorBreizh  & Intégration d'applicatifs clients (JBOSS, Tomcat)
    & Intégration de Cluster Linux (HeartBeat/RedHat) en environnement de production
    & Création de scripts d'installation et déploiement automatisés ou interactifs
    & Migration de serveurs et applicatifs en environnement de production
    & Tests préparatoires de migration et d'automatisation
\textbf{Projet:~\textit{Gentiane  & Intégration Cluster Linux (HeartBeat/RedHat)
    & Mise en œuvre du stockage SAN avec accès concurrent pour le Cluster Linux (OCFS)
\textbf{Projet:~\textit{Vauban & Tests de migration d'OS Linux 32bits vers 64bits
    & Préparation des outils de migration (disques, software)
    & Migration de plateforme applicative en environnement de production
    & Diagnostics et résolution de problèmes post-migration
\textbf{Projet:~\textit{Itinisère  & Définition de l’Architecture de l'infrastructure du projet
    & Dimensionnement et validation des infrastructures systèmes et cluster ESXi
    & de l'infrastructure réseau LAN et MAN, et du dimensionnement de la solution de stockage SAN
    & Création des protocoles d'installation et d'intégration
    & Design général et détaillé de l'infrastructure
Rôles et activités  & Architecture et intégration système, réseau et applicative
Environnement technique & VMware ESXi, Windows Server 2008, Redhat, CentOS, Cluster, VSphere, Stockage, Apache, Centreon
 &


### Helice, SFR
> Juil. 2010 à Mai 2011, Lyon
Poste      & Gestion technique de Datacenter
Mission    & Installations, survey, management de salles, interventions d'urgence (astreintes)
Env. Technique    & Datacenter
 &


### Axians, interim
> Fév. à Juil. 2010, Lyon
Poste       & Technicien réseaux
Mission    & Migrations switchs et routeurs en environement de production, installation ToIP
Env. Technique    & HP Procurve, Routeurs Cisco
 &


### Institut de Physique Nucleaire de Lyon (UMR CNRS-UCBL)
> Sept. à Déc. 2009, Lyon
Poste       & Support utilisateur Linux (vacataire)
Mission     & Installation, maintenance, réparation postes utilisateurs
Technique Env.    & Scientific Linux, RedHat Enterprise Linux, Ubuntu Linux
Réalisations    & \'{Etude de l'architecture d'un SunFire X4500 pour utilisation en ferme de calcul
 &


### Medimex, alternance
> Sept. 2007 à Juil. 2009, Lyon
Contexte    & Spécialiste de l’évaluation fonctionnelle et de la rééducation.
Projet      & Refonte complète de l'infrastructure système et réseau de l'entreprise
Rôles et activités   & Gestion de l'ensemble du parc machines (serveurs, postes client)
    & Administration du réseau informatique et téléphonique sur IP
    & Mise à jour, maintien en condition opérationnelle et évolution des serveurs
    & Mise en place de politiques de sécurité
    & Mise en place de politiques de sauvegarde
    & Mise en place de plan de reprise d'activité
    & Création de scripts d'industrialisation et d'installation
    & Rédaction de documentation exhaustive sur l'infrastructure du système d'information
    & Mise à l'épreuve des politiques de sauvegarde et de reprise d'activité
    & Formation et support aux utilisateurs
Env. technique    & Windows, Linux Debian, Xen, Virtualisation, sécurité, Apache
 &


## Formations
* **2015** Puppet v4 - _Orsys_
* **2011** ITILv3 Foundation - _FCT Solutions_
* **2007-2009** Technicien Supérieur en Maintenance Informatique et Réseaux - _Ciefa Lyon_


## Projets personnels
### Sakeyra
Outil de gestion et diffusion des clés SSH publiques au sein d'un écosystème de serveurs GNU/Linux.

**_actuellement en refonte_**

### [Akeyra](https://gitlab.com/LaMethode/akeyra/)
Client du serveur Sakeyra
